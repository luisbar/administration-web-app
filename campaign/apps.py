# Django modules
from django.apps import AppConfig
# For customizing some stuffs about the campaign app
class CampaignConfig(AppConfig):
    name = 'campaign'
