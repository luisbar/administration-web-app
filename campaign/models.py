# From python
import os
# From django
from django.db import models
# From project
from enterprise.models import Enterprise


# Header of a campaign
class Campaign(models.Model):
    title = models.CharField(max_length=50)
    message = models.CharField(max_length=5)
    image = models.ImageField(upload_to='campaign', null=True, blank=True)
    enterprise = models.ForeignKey(Enterprise, on_delete=models.DO_NOTHING, related_name='enterprise')
    status = models.CharField(max_length=20)
    # To delete the image file
    def delete(self):
        if self.image:
            os.remove(self.image.path)

        return super(Campaign, self).delete()


# Detail of a campaign
class CampaignDetail(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, related_name='campaign')
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    sent = models.BooleanField(default=False)
    read = models.BooleanField(default=False)
    sent_time = models.DateTimeField(null=True)
    read_time = models.DateTimeField(null=True)
