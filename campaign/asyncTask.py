# From python
import re
from threading import Thread
# From project
from .models import CampaignDetail

class AsyncTask(Thread):

    def  __init__(self, campaign, csv_file):
        Thread.__init__(self)
        self.campaign = campaign
        self.csv_file = csv_file

    def run(self):
        campaign_detail_list = []

        with self.csv_file as csv_file:
            for phone_number in csv_file:
                phone_number = re.sub('\D', '', str(phone_number))
                campaign_detail = CampaignDetail(campaign=self.campaign, phone_number=phone_number)
                campaign_detail_list.append(campaign_detail)

        CampaignDetail.objects.bulk_create(campaign_detail_list)
