# From django
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy, reverse
from django.shortcuts import redirect, render
from django.core.paginator import Paginator
# From project
from .models import Campaign, CampaignDetail
from enterprise.models import Enterprise
from .forms import CampaignForm
from .asyncTask import AsyncTask


# It renders the list of campaigns
class CampaignsView(ListView):
    model = Campaign
    template_name = 'campaign/campaigns.html'
    paginate_by = 20
    context_object_name = 'campaigns'
    ordering =  ['-id']# Descending


# It renders a campaign detailed
class CampaignDetailView(DetailView):
    model = Campaign
    template_name = 'campaign/campaign.html'
    paginate_by = 20
    # To paginate the related model
    def get_context_data(self, **kwargs):
        context = super(CampaignDetailView, self).get_context_data(**kwargs)

        number_of_page = self.request.GET.get('page', 1)
        campaign = Campaign.objects.get(id=self.kwargs['pk'])

        paginator = Paginator(campaign.campaign.all(), self.paginate_by)
        page_obj = paginator.page(number_of_page)

        max = int(self.paginate_by) * int(number_of_page)
        min = max - int(self.paginate_by)
        detail = campaign.campaign.all()[min:max]

        context['detail'] = detail
        context['page_obj'] = page_obj
        context['is_paginated'] = True

        return context


# It renders the form for registering a campaign
class CampaignRegistrationView(CreateView):
    model = Campaign
    fields = '__all__'
    template_name_suffix = '_registration_form'

    def dispatch(self, request, *args, **kwargs):

        try:
            if request.method == 'POST' :
                campaign_form = CampaignForm(request.POST, request.FILES)

                if campaign_form.is_valid():
                    enterprise = Enterprise.objects.get(pk=request.POST.get('enterprise'))
                    campaign = Campaign(title=request.POST.get('title', ''), message=request.POST.get('message', ''), enterprise=enterprise, status='No enviado', image=request.FILES.get('image', ''))
                    campaign.save()

                    AsyncTask(campaign, request.FILES['phone_numbers']).start()

                    return redirect(reverse('add-campaign') + '?success')
                else:
                    return render(request, 'campaign/campaign_registration_form.html', { 'form': campaign_form })
            else:
                return render(request, 'campaign/campaign_registration_form.html', { 'form': CampaignForm() })
        except Exception as e:
            return redirect(reverse('add-campaign') + '?fail')


# It renders the form for deleting a campaign
class CampaignDeletionView(DeleteView):
    model = Campaign
    success_url = reverse_lazy('home')
    template_name_suffix = '_deletion_form'
