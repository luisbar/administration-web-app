$('input:file').on('change',function(){
    //get the file name
    var pathSplitted = $(this).val().split('\\')
    fileName = pathSplitted[pathSplitted.length-1]
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
})