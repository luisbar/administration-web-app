# From django
from django.urls import path
from django.contrib.auth.decorators import login_required
# From project
from .views import CampaignsView, CampaignRegistrationView, CampaignDeletionView, CampaignDetailView


urlpatterns = [
    path('', login_required(CampaignsView.as_view()), name='home'),
    path('campaign/<int:pk>', login_required(CampaignDetailView.as_view()), name='campaign-detail'),
    path('campaign/add/', login_required(CampaignRegistrationView.as_view()), name='add-campaign'),
    path('campaign/delete/<int:pk>', login_required(CampaignDeletionView.as_view()), name='delete-campaign'),
]