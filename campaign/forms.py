# From django
from django import forms
# From project
from .models import Campaign

class CampaignForm(forms.ModelForm):
    phone_numbers = forms.FileField(
        widget = forms.FileInput(
            attrs = {
                'class': 'custom-file-input',
                'accept': '.csv',
                'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                'onchange': 'this.setCustomValidity(\'\')',
            }
        ),
        error_messages = { 'required': 'Este campo es obligatorio.' },
        label = 'Números de telefono',
    )

    class Meta:
        model = Campaign
        fields = ['message', 'image', 'title', 'enterprise']
        labels = {
            'title': 'Título',
            'message': 'Mensaje',
            'image': 'Imagen',
            'enterprise': 'Empresa'
        }
        widgets = {
            'title': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Escribir título',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                },
            ),
            'message': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Escribir mensaje',
                    'rows': 5,
                    'style': 'resize: none;',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                },
            ),
            'image': forms.FileInput(
                attrs = {
                    'class': 'custom-file-input',
                }
            ),
            'enterprise': forms.Select(
                attrs = {
                    'class': 'form-control',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                },
            ),
        }
    def clean_phone_numbers(self):
        phone_numbers = self.cleaned_data.get('phone_numbers');
        # TODO: validate csv file
        return phone_numbers
