function toggleDrawer() {
    $(".drawer").toggleClass("drawer-opened");
    $(".drawer-container").toggleClass("drawer-opened");
    $(".drawer-container-mobile").toggleClass("drawer-opened");
    $(".drawer-button").toggleClass("was-pressed");
    $(".drawer-button-mobile").toggleClass("was-pressed");
}
