# From python
import os
# From django
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save

def custom_upload_to(instance, filename):
    if instance.pk != None:
        old_instance = Enterprise.objects.get(pk=instance.pk)
        old_instance.image.delete()

    return 'enterprise/' + filename

class Enterprise(models.Model):
    name = models.CharField(max_length=150, unique=True, verbose_name='nombre')
    image = models.ImageField(upload_to=custom_upload_to)
    status = models.CharField(max_length=700)
    newImage = models.BooleanField(default=True)
    newStatus = models.BooleanField(default=True)

    def delete(self):
        if self.image:
            os.remove(self.image.path)

        return super(Enterprise, self).delete()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'empresa'

@receiver(pre_save, sender=Enterprise)
def check_if_image_or_status_has_changed(sender, instance, **kwargs):
    current_instance = Enterprise.objects.filter(pk=instance.pk)

    if current_instance:
        current_instance = current_instance[0]
        if current_instance.image != instance.image:
            instance.newImage = True
        if current_instance.status != instance.status:
            instance.newStatus = True
