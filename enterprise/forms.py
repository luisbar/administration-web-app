# From django
from django import forms
# From project
from .models import Enterprise

class EnterpriseForm(forms.ModelForm):
    class Meta:
        model = Enterprise
        fields = ['name', 'image', 'status']
        labels = {
            'name': 'Nombre',
            'status': 'Estado',
            'image': 'Imagen',
        }
        widgets = {
            'name': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Escribir nombre',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                },
            ),
            'status': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Escribir estado',
                    'rows': 5,
                    'style': 'resize: none;',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                },
            ),
            'image': forms.FileInput(
                attrs = {
                    'class': 'custom-file-input',
                    'oninvalid': 'this.setCustomValidity(\'Este campo es obligatorio.\')',
                    'onchange': 'this.setCustomValidity(\'\')',
                }
            ),
        }
