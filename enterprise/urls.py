# From django
from django.urls import path
from django.contrib.auth.decorators import login_required
# From project
from .views import EnterprisesView, EnterpriseRegistrationView, EnterpriseDeletionView, EnterpriseDetailView, EnterpriseUpdateView


urlpatterns = [
    path('enterprises', login_required(EnterprisesView.as_view()), name='enterprises'),
    path('enterprise/<int:pk>', login_required(EnterpriseDetailView.as_view()), name='enterprise-detail'),
    path('enterprise/add/', login_required(EnterpriseRegistrationView.as_view()), name='add-enterprise'),
    path('enterprise/delete/<int:pk>', login_required(EnterpriseDeletionView.as_view()), name='delete-enterprise'),
    path('enterprise/update/<int:pk>', login_required(EnterpriseUpdateView.as_view()), name='update-enterprise'),
]
