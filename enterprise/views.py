# From django
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy, reverse
from django import forms
# From project
from .models import Enterprise
from .forms import EnterpriseForm


# It renders the list of enterprises
class EnterprisesView(ListView):
    model = Enterprise
    template_name = 'enterprise/enterprises.html'
    paginate_by = 20
    context_object_name = 'enterprises'
    ordering =  ['-id']# Descending

# It renders a enterprise detailed
class EnterpriseDetailView(DetailView):
    model = Enterprise
    template_name = 'enterprise/enterprise.html'


# It renders the form for registering a enterprise
class EnterpriseRegistrationView(CreateView):
    model = Enterprise
    template_name_suffix = '_registration_form'
    form_class = EnterpriseForm

    def get_success_url(self):
        return reverse('add-enterprise') + '?success'


# It renders the form for deleting a enterprise
class EnterpriseDeletionView(DeleteView):
    model = Enterprise
    success_url = reverse_lazy('enterprises')
    template_name_suffix = '_deletion_form'


# It renders the form for updating a enterprise
class EnterpriseUpdateView(UpdateView):
    model = Enterprise
    template_name_suffix = '_update_form'
    form_class = EnterpriseForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['enterprise'] = Enterprise.objects.get(pk=self.object.pk)
        return context

    def get_success_url(self):
        return reverse('update-enterprise', kwargs={'pk': self.object.pk}) + '?success'

    def get_form(self, form_class=None):
        form = super(EnterpriseUpdateView, self).get_form()

        form.fields['image'].required = False
        form.fields['image'].widget = forms.FileInput(
            attrs = {
                'class': 'custom-file-input',
            }
        )

        return form;
